#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QImageReader>
#include <QMessageBox>
#include <QStandardPaths>
#include <QDebug>
#include <QRgb>
#include <QRgb>
#include <QPaintEngine>
#include <QDir>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->progressBar->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::loadImage(const QString &filePath, QImage *pImg)
{
    QImageReader reader(filePath);
    reader.setAutoTransform(true);
    *pImg = reader.read();
    if (pImg->isNull()) {
        QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                                 tr("Cannot load %1: %2")
                                 .arg(QDir::toNativeSeparators(filePath), reader.errorString()));
        return false;
    }

    return true;
}

void MainWindow::setImage(const QImage &newImage, QLabel *pLabel)
{
    pLabel->setPixmap(QPixmap::fromImage(newImage.scaled(pLabel->size(), Qt::KeepAspectRatio)));
}

void MainWindow::reset()
{
    if(!m_ResultImg.isNull())
        setImage(m_ResultImg = QImage(), ui->label_ResultImgDisplay);
}

void MainWindow::on_pushButton_browseSrc_clicked()
{
    requestImage(ui->lineEdit_SrcImgPath, &m_SrcImg, ui->label_SrcImgDisplay);
}

void MainWindow::on_pushButton_browseCmp_clicked()
{
    requestImage(ui->lineEdit_CmpImgPath, &m_CmpImg, ui->label_CmpImgDisplay);
}

void MainWindow::requestImage(QLineEdit *pEditBox, QImage *pImageDst, QLabel *pDisplayLabel)
{
    // request path
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"), QDir::currentPath(), tr("Image Files (*.png *.jpg *.bmp)"));
    if(fileName.length() == 0)
        return;

    reset();

    // set editbox contents
    pEditBox->setText(fileName);

    // display image
    loadImage(fileName, pImageDst);
    qDebug() << fileName << pImageDst->format() << " size " << pImageDst->size();
    setImage(*pImageDst, pDisplayLabel);
}

void MainWindow::on_pushButton_Go_clicked()
{
    bool error = false;
    const QString redBg = "QLineEdit { background: red; selection-background-color: rgb(233, 99, 0); }";
    if(m_SrcImg.isNull())
    {
        error = true;
        ui->lineEdit_SrcImgPath->setStyleSheet(redBg);
    }

    if(m_CmpImg.isNull())
    {
        error = true;
        ui->lineEdit_CmpImgPath->setStyleSheet(redBg);
    }

    if(error)
        return;

    lockGui();

    m_ResultImg = QImage(m_SrcImg.size()-m_CmpImg.size(), QImage::Format_RGB888);
    m_ResultImg.fill(QColor(0,0,0));
    calculateDifferences();
    setImage(m_ResultImg, ui->label_ResultImgDisplay);

    unlockGui();
}

void MainWindow::lockGui()
{
    ui->lineEdit_SrcImgPath->setEnabled(false);
    ui->lineEdit_CmpImgPath->setEnabled(false);
    ui->pushButton_browseSrc->setEnabled(false);
    ui->pushButton_browseCmp->setEnabled(false);
    ui->pushButton_Go->hide();

    ui->progressBar->show();
    ui->progressBar->setValue(0);
}

void MainWindow::unlockGui()
{
    ui->lineEdit_SrcImgPath->setEnabled(true);
    ui->lineEdit_CmpImgPath->setEnabled(true);
    ui->pushButton_browseSrc->setEnabled(true);
    ui->pushButton_browseCmp->setEnabled(true);
    ui->pushButton_Go->show();

    ui->progressBar->hide();
}

void MainWindow::on_lineEdit_SrcImgPath_textChanged(const QString &)
{
    tryLoadImage(ui->lineEdit_SrcImgPath, &m_SrcImg, ui->label_SrcImgDisplay);
}

void MainWindow::on_lineEdit_CmpImgPath_textChanged(const QString &)
{
    tryLoadImage(ui->lineEdit_CmpImgPath, &m_CmpImg, ui->label_CmpImgDisplay);
}

void MainWindow::tryLoadImage(QLineEdit *pEditBox, QImage *pImageDst, QLabel *pDisplayLabel)
{
    reset();

    const QString& fileName = pEditBox->text();
    pEditBox->setStyleSheet("");
    if(QFile(fileName).exists())
    {
        qDebug() << "found " << fileName;

        // display image
        loadImage(fileName, pImageDst);
        setImage(*pImageDst, pDisplayLabel);
    }
    else
        pDisplayLabel->clear();
}

void MainWindow::calculateDifferences()
{
    int lastProgressPct = 0;
    const unsigned int maxProgress = (unsigned)(m_SrcImg.height()-m_CmpImg.height()) * (unsigned)(m_SrcImg.width()-m_CmpImg.width());

    double minResult = INT_MAX;
    double maxResult = 0;
    QPoint maxPos;

    // schiebe das cmp-img durch das ganze src-img durch
    const int yMax = m_SrcImg.height()-m_CmpImg.height();
    const int xMax = m_SrcImg.width()-m_CmpImg.width();
    //int *pResultTable = new int[yMax*xMax];
    double aResultTable[yMax][xMax];
    for(int y = 0; y < yMax; y++)
    {
        for(int x = 0; x < xMax; x++)
        {
            double res = calculateCorrelation(x, y);
            if(res < minResult)
                minResult = res;
            if(res > maxResult)
            {
                maxResult = res;
                maxPos = QPoint(x, y);
            }

            // save res val for later
            aResultTable[y][x] = res;

            // advance the progress bar
            int progressPct = (int)((((double)((y+1)*(x+1))) / (double)maxProgress)*100.0);
            if(progressPct > lastProgressPct)
            {
                lastProgressPct = progressPct;
                ui->progressBar->setValue(progressPct);
            }
        }
    }

    // loop through the result array, map all values and create the result image
    for(int y = 0; y < yMax; y++)
    {
        for(int x = 0; x < xMax; x++)
        {
            // re-map res from -1..1 to 0..255
            int grayVal = (int)map_value(aResultTable[y][x], minResult, maxResult, 0x00, 0xFF);
            //qDebug() << asdf << " @ " << QPoint(x, y);
            m_ResultImg.setPixelColor(x, y, QColor(grayVal,grayVal,grayVal));

            if(grayVal < 0 || grayVal > 255)
            {
                qDebug() << "ERROR " << grayVal;
                return;
            }

        }
    }

    // mark the found area
    m_SrcImg = m_SrcImg.convertToFormat(QImage::Format_RGB888);
    for(int x = -m_CmpImg.width()/2; x < m_CmpImg.width()/2; x++)
    {
        for(int y = -m_CmpImg.height()/2; y < m_CmpImg.height()/2; y++)
        {
            int posX = maxPos.x()+x+m_CmpImg.width()/2;
            int posY = maxPos.y()+y+m_CmpImg.height()/2;

            const QColor& pixelVal = m_SrcImg.pixel(posX, posY);
            QColor mixed = QColor(
                        (int)mix(pixelVal.red(), 255, 0.5),
                        (int)mix(pixelVal.green(), 0, 0.5),
                        (int)mix(pixelVal.blue(), 0, 0.5)
            );

            m_SrcImg.setPixelColor(posX, posY, mixed);
        }
    }
       setImage(m_SrcImg, ui->label_SrcImgDisplay);

    qDebug() << "fertig, min is " <<  minResult << " und max is " << maxResult << " @ " << maxPos;
}

double MainWindow::calculateCorrelation(int dx, int dy)
{
    // iteriere über alle punkte im cmp-img
    double result = 0;
    for(int y = 0; y < m_CmpImg.height(); y++)
    {
        for(int x = 0; x < m_CmpImg.width(); x++)
        {
            const QColor& srcColor = m_SrcImg.pixel(dx+x, dy+y);
            const QColor& cmpColor = m_CmpImg.pixel(x, y);
            double srcColorNormalised = map_value(srcColor.black(), 0x00, 0xFF, -1, +1);
            double cmpColorNormalised = map_value(cmpColor.black(), 0x00, 0xFF, -1, +1);
            double pixelResult = srcColorNormalised * cmpColorNormalised;
            result += pixelResult;
            //qDebug("x=%i y=%i dx=%i dy=%i res=%i", dx, dy, x, y, Result);
        }
    }

    return result;
}

double MainWindow::map_value(double input, double input_start, double input_end, double output_start, double output_end)
{
    double slope = 1.0 * (output_end - output_start) / (input_end - input_start);
    double output = output_start + slope * (input - input_start);
    return output;
}

double MainWindow::mix(double a, double b, double amount)
{
    return a*(1.0-amount) + b*(amount);
}
