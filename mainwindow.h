#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    QImage m_SrcImg;
    QImage m_CmpImg;
    QImage m_ResultImg;

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    bool loadImage(const QString& filePath, QImage *pImg);
    void setImage(const QImage& image, class QLabel *pLabel);
    void requestImage(class QLineEdit *editBox, QImage *pImageDst, QLabel *pDisplayLabel);
    void tryLoadImage(class QLineEdit *pEditBox, QImage *pImageDst, QLabel *pDisplayLabel);
    void reset();

    static double map_value(double input, double input_start, double input_end, double output_start, double output_end);
    static double mix(double a, double b, double amount);

private slots:
    void on_pushButton_browseSrc_clicked();

    void on_pushButton_browseCmp_clicked();

    void on_pushButton_Go_clicked();

    void on_lineEdit_SrcImgPath_textChanged(const QString &arg1);

    void on_lineEdit_CmpImgPath_textChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;

    void calculateDifferences();
    double calculateCorrelation(int dx, int dy);

    void lockGui();
    void unlockGui();
};

#endif // MAINWINDOW_H
